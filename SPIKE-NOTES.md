# DSC :: Ember Engines Spike

- [Ember Engines Guide](http://ember-engines.com/guide)
- [Ember Engines on GitHub](https://github.com/ember-engines/ember-engines)


## Create an Ember Engine

- Currently, no blueprints for engines.  Engines are distributed as Addons. To create, do this:

```bash
ember addon <engine-name>
```

- In that newly created directory, install the `ember-engines` addon:

```bash
cd <engine-name>/
ember install ember-engines
```

- You must have compatibility between Ember Engines and Ember itself. [See this README](https://github.com/ember-engines/ember-engines/blob/master/README.md#important-note-about-compatibility-and-stability) for compatibility information.


### Dependencies

- In the Engine addon, `ember-engines` needs to be a `devDependency` to avoid issues.

- In the consuming application, `ember-engines` needs to be a *primary* `dependency`.

- In both the Engine and consuming app, `ember-cli-htmlbars` needs to be a `dependency`.


## Configuring an Ember Engine

- Within your Engine's root directory, modify `index.js` so that your addon is configured as an engine using the EngineAddon extension:

```js
// index.js

/*jshint node:true*/
const EngineAddon = require('ember-engines/lib/engine-addon');

module.exports = EngineAddon.extend({
  name: '', // <-- the name of the generated engine
  lazyLoading: false
});
```

- Within your Engine's `config` directory, modify the `environment.js` file:

```js
// config/environment.js

/*jshint node:true*/
'use strict';

module.exports = function(environment) {
  const ENV = {
    modulePrefix: '', // <-- the name of the generated engine
    environment: environment
  }

  return ENV;
};
```

- To make this addon an actual Engine, create a `engine.js` file under the `addon` directory:

```bash
touch addon/engine.js
```

- Add the following code to `engine.js`:

```js
// addon/engine.js

import Engine from 'ember-engines/engine';
import Resolver from 'ember-resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

const { modulePrefix } = config;
const Eng = Engine.extend({
  modulePrefix,
  Resolver
});

loadInitializers(Eng, modulePrefix);

export default Eng;
```

- Ember Engines can be routeless, or routable. For the purposes of this spike, they are routable. Do this:

```bash
touch addon/routes.js
```

```js
// addon/routes.js

import buildRoutes from 'ember-engines/routes';

export default buildRoutes(function() {
  this.route('index', { path: '/' });
});
```


## Mounting an Engine in the Consuming App

- Mounting a routable engine is done through the consuming app's `Router` map, like so:

```js
Router.map(function() {
  this.mount('some-engine');
});
```

- The `mount` method is very similar to the `route` method you would normally use to define routes. You can specify a `path` and `resetNamespace` options like you normally would:

```js
Router.map(function() {
  this.mount('some-engine', { resetNamespace: true, path: '/some-engine' });
});
```

You can also specify a special property, `as`, which allows you to mount more than one instance of an Engine by specifying the route name it'll live under:

```js
Router.map(function() {
  this.mount('some-engine', { path: '/some-engine' });
  this.mount('some-engine', { as: 'dev-some-engine', path: '/dev-some-engine' });
});
```


# Findings

## Prototype Experiment

- For the purposes of running through a setup and "seeing it work", I created a simple Ember Engine (eager, routable) and mounted it on a route in my website - a simple, non-complex, Ember app.

- The biggest issue I ran into, was compatibility between Ember Engines and the Ember version that was being used.  The compatibility notes are no joke, as subsequent Engine versions definitely introduce breaking changes.

- That aside, the setup was straightforward, and the engine mounted as expected.  I could hit the route, and see that the engine successfully provided layout, styles, and images.  Huzzah.

### Face-web Experiment

- For a test of Ember Engines in Face-web, I wanted to see if I could mount an Ember Engine called face-gifting, on a route with a path value of `face-gifting-engine`.

- The same setup was followed, but it should be noted that different versions of Ember and Ember Engines were in play with the 2 experiments.

- The following errors were encountered, which Arjan felt were similar to what was encountered when setting up Fastboot - that these are primarily Ember setup issues within the Face-web app itself.

![](public/assets/images/unknown-injections-errors.png?raw=true)

- The spike was concluded at this point.
