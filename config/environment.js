/* eslint-env node */
'use strict';

module.exports = function(environment) {
  const ENV = {
    modulePrefix: 'face-gifting',
    environment: environment
  }

  return ENV;
};
